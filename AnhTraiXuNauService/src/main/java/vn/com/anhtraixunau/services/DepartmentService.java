package vn.com.anhtraixunau.services;

import java.util.List;

import vn.com.anhtraixunau.models.Department;
import vn.com.anhtraixunau.repositories.DepartmentDAO;
import vn.com.anhtraixunau.repositories.DepartmentDAOImpl;

public class DepartmentService {
	private DepartmentDAO departmentDAO;
	
	public List<Department> getListDepartmentExisting(){
		try {
			departmentDAO = new DepartmentDAOImpl();
					
			List<Department> listDepartment = departmentDAO.getListDepartmentExisting();
			
			return listDepartment;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}
	
	public Department getDepartmentById(int id) {
		try {
			departmentDAO = new DepartmentDAOImpl();
			
			Department department = departmentDAO.getDepartmentById(id);
			
			return department;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}
	
	public int insertDepartment(String name, String userName) {
		try {
			departmentDAO = new DepartmentDAOImpl();
			
			int result = departmentDAO.insertDepartment(name, userName);
			
			return result;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return -1;
		}
	}
	
	public int updateDepartment(int id, String name, String userName) {
		try {
			departmentDAO = new DepartmentDAOImpl();
			
			int result = departmentDAO.updateDepartment(id, name, userName);
			
			return result;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return -1;
		}
	}
	
	public int deleteDepartmentById(int id, String userName) {
		try {
			departmentDAO = new DepartmentDAOImpl();
			
			int result = departmentDAO.deleteDepartmentById(id, userName);
			
			return result;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return -1;
		}
	}
}
