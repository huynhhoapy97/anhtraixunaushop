package vn.com.anhtraixunau.services;

import java.util.List;

import vn.com.anhtraixunau.models.StaffPermission;
import vn.com.anhtraixunau.repositories.StaffPermissionDAO;
import vn.com.anhtraixunau.repositories.StaffPermissionDAOImpl;

public class StaffPermissionService {
private StaffPermissionDAO staffPermissionDAO;
	
	public List<StaffPermission> getListStaffPermissionExisting(){
		try {
			staffPermissionDAO = new StaffPermissionDAOImpl();
					
			List<StaffPermission> listStaffPermission = staffPermissionDAO.getListStaffPermissionExisting();
			
			return listStaffPermission;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}
	
	public StaffPermission getStaffPermissionById(int id) {
		try {
			staffPermissionDAO = new StaffPermissionDAOImpl();
			
			StaffPermission staffPermission = staffPermissionDAO.getStaffPermissionById(id);
			
			return staffPermission;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return null;
		}
	}
	
	public int insertStaffPermission(int departmentId, String name, String userName) {
		try {
			staffPermissionDAO = new StaffPermissionDAOImpl();
			
			int result = staffPermissionDAO.insertStaffPermission(departmentId, name, userName);
			
			return result;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return -1;
		}
	}
	
	public int updateStaffPermission(int departmentId, int id, String name, String userName) {
		try {
			staffPermissionDAO = new StaffPermissionDAOImpl();
			
			int result = staffPermissionDAO.updateStaffPermission(departmentId, id, name, userName);
			
			return result;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return -1;
		}
	}
	
	public int deleteStaffPermissionById(int id, String userName) {
		try {
			staffPermissionDAO = new StaffPermissionDAOImpl();
			
			int result = staffPermissionDAO.deleteStaffPermissionById(id, userName);
			
			return result;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return -1;
		}
	}
}
