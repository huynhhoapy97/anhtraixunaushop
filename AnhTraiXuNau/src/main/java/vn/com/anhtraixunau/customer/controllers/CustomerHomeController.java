package vn.com.anhtraixunau.customer.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CustomerHomeController {
	@GetMapping("index")
	public String index() {
		
		return "index";
	}
}
