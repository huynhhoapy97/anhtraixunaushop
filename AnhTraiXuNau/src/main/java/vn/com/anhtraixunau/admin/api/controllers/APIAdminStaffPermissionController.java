package vn.com.anhtraixunau.admin.api.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.com.anhtraixunau.enums.StaffPermissionMessage;
import vn.com.anhtraixunau.models.Department;
import vn.com.anhtraixunau.models.StaffPermission;
import vn.com.anhtraixunau.services.DepartmentService;
import vn.com.anhtraixunau.services.StaffPermissionService;

@RestController
@RequestMapping("api/admin/staffPermission")
public class APIAdminStaffPermissionController {
	private StaffPermissionService staffPermissionService;
	private DepartmentService departmentService;
	
	@GetMapping("getListStaffPermissionExisting")
	public Map<String, Object> getListStaffPermissionExisting() {
		Map<String, Object> response = new HashMap<String, Object>();
		List<StaffPermission> listStaffPermission = new ArrayList<StaffPermission>();
		
		try {
			staffPermissionService = new StaffPermissionService();
			listStaffPermission = staffPermissionService.getListStaffPermissionExisting();
			
			response.put("listStaffPermission", listStaffPermission);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	@GetMapping("getStaffPermissionById/{id}")
	public Map<String, Object> getStaffPermissionById(@PathVariable("id") Integer staffPermissionId) {
		Map<String, Object> response = new HashMap<String, Object>();
		StaffPermission staffPermission = new StaffPermission();
		
		try {
			staffPermissionService = new StaffPermissionService();
			staffPermission = staffPermissionService.getStaffPermissionById(staffPermissionId);
			
			response.put("staffPermission", staffPermission);
		}
		catch (Exception e) {
			e.printStackTrace();
			
			response.put("message", "Lỗi lấy thông tin quyền thao tác");
		}
		
		return response;
	}
	
	@PostMapping("completeStaffPermissionEdit")
	public Map<String, Object> completeStaffPermissionEdit(@RequestBody StaffPermission staffPermission, HttpServletRequest httpServletRequest) {
		Map<String, Object> response = new HashMap<String, Object>();
		staffPermissionService = new StaffPermissionService();
		departmentService = new DepartmentService();
		
		try {
			int departmentId = staffPermission.getDepartmentId();
			int staffPermissionId = staffPermission.getId();
			String staffPermissionName = staffPermission.getName();
			String userName = (String) httpServletRequest.getSession().getAttribute("username");
			
			if (staffPermissionId == StaffPermissionMessage.DOES_NOT_EXISTS_STAFFPERMISSIONID.getId()) {
				staffPermissionService.insertStaffPermission(departmentId, staffPermissionName, userName);
			}
			else {
				staffPermissionService.updateStaffPermission(departmentId, staffPermissionId, staffPermissionName, userName);
			}
			
			List<StaffPermission> listStaffPermission = new ArrayList<StaffPermission>();
			listStaffPermission = staffPermissionService.getListStaffPermissionExisting();
			
			List<Department> listDepartment = new ArrayList<Department>();
			listDepartment = departmentService.getListDepartmentExisting();
			
			response.put("listStaffPermission", listStaffPermission);
			response.put("listDepartment", listDepartment);
		}
		catch (Exception e) {
			e.printStackTrace();
			
			response.put("message", "Lỗi thêm/sửa thông tin quyền thao tác");
		}
		
		return response;
	}
	
	@PostMapping("deleteStaffPermissionById/{id}")
	public Map<String, Object> deleteStaffPermissionById(@PathVariable("id") Integer id, HttpServletRequest httpServletRequest) {
		Map<String, Object> response = new HashMap<String, Object>();
		staffPermissionService = new StaffPermissionService();
		departmentService = new DepartmentService();
		
		try {
			String userName = (String) httpServletRequest.getSession().getAttribute("username");
			staffPermissionService.deleteStaffPermissionById(id, userName);
			
			List<StaffPermission> listStaffPermission = new ArrayList<StaffPermission>();
			listStaffPermission = staffPermissionService.getListStaffPermissionExisting();
			
			List<Department> listDepartment = new ArrayList<Department>();
			listDepartment = departmentService.getListDepartmentExisting();
			
			response.put("listStaffPermission", listStaffPermission);
			response.put("listDepartment", listDepartment);
		}
		catch (Exception e) {
			e.printStackTrace();
			
			response.put("message", "Lỗi xóa thông tin quyền thao tác");
		}
		
		return response;
	}
}
