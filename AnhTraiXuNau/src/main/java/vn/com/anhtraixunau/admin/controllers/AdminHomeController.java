package vn.com.anhtraixunau.admin.controllers;

import java.io.File;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FilenameUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import vn.com.anhtraixunau.enums.AdminAccountMessage;
import vn.com.anhtraixunau.models.AdminAccount;
import vn.com.anhtraixunau.models.Category;
import vn.com.anhtraixunau.models.Department;
import vn.com.anhtraixunau.services.AdminAccountService;
import vn.com.anhtraixunau.services.DepartmentService;

@Controller
@RequestMapping("admin")
public class AdminHomeController {
	
	private AdminAccountService adminAccountService;
	private DepartmentService departmentService;
	private String viewName;
	private String pageName;
	private String message;
	
	@Autowired
	private ServletContext servletContext;
	
	@GetMapping("index")
	public String index(ModelMap modelMap) {
		viewName = "admin/index";
		pageName = "login.jsp";
		message = "";

		modelMap.addAttribute("adminAccount", new AdminAccount());
		
		return view(modelMap, viewName, pageName, message);
	}
	
	@PostMapping("login")
	public String login(@Validated @ModelAttribute("adminAccount") AdminAccount adminAccount, BindingResult bindingResult, ModelMap modelMap) {
		viewName = "admin/index";
		pageName = "login.jsp";
		message = "";

		if (bindingResult.hasErrors()) {
			pageName = "error.jsp";
		}
		else {
			adminAccountService = new AdminAccountService();
			int result = adminAccountService.checkAccountExists(adminAccount);
			
			if (result == AdminAccountMessage.MESSAGE_ERROR_PASSWORD.getId()) {
				pageName = "changepassword.jsp";
			}
			else if (result == AdminAccountMessage.MESSAGE_NO_ERROR.getId()) {
				pageName = "confirmpassword.jsp";
			}
			else {
				for (AdminAccountMessage adminAccountMessage : AdminAccountMessage.values()) {
					if (result == adminAccountMessage.getId()) {
						message = adminAccountMessage.getMessage();
						
						break;
					}
				}
			}
		}
		
		return view(modelMap, viewName, pageName, message);
	}
	
	@PostMapping("changePassword")
	public String changePassword(@Validated @ModelAttribute("adminAccount") AdminAccount adminAccount, BindingResult bindingResult, ModelMap modelMap) {
		viewName = "admin/index";
		pageName = "changepassword.jsp";
		message = "";
		
		if (bindingResult.hasErrors()) {
			pageName = "error.jsp";
		}
		else {
			adminAccountService = new AdminAccountService();
			int result = adminAccountService.changePassword(adminAccount);
			
			if (result == AdminAccountMessage.MESSAGE_NO_ERROR.getId()) {
				pageName = "confirmpassword.jsp";
			}
			else {
				for (AdminAccountMessage adminAccountMessage : AdminAccountMessage.values()) {
					if (result == adminAccountMessage.getId()) {
						message = adminAccountMessage.getMessage();
						
						break;
					}
				}
			}
		}
		
		return view(modelMap, viewName, pageName, message);
	}
	
	@PostMapping("confirmPassword")
	public String confirmPassword(@Validated @ModelAttribute("adminAccount") AdminAccount adminAccount, BindingResult bindingResult, ModelMap modelMap, HttpServletRequest httpServletRequest) {
		viewName = "admin/index";
		pageName = "confirmpassword.jsp";
		message = "";
		
		if (bindingResult.hasErrors()) {
			pageName = "error.jsp";
		}
		else {
			adminAccountService = new AdminAccountService();
			int result = adminAccountService.confirmPassword(adminAccount);
			
			if (result == AdminAccountMessage.MESSAGE_NO_ERROR.getId()) {
				pageName = "dashboard.jsp";
				viewName = "admin/dashboard";
				
				// Lưu Session user đăng nhập
				HttpSession httpSession = httpServletRequest.getSession();
				
				// Kiểm tra xem có Session user đăng nhập chưa?
				if (httpSession.getAttribute("username") == null) {
					// Chưa tồn tại Session user đăng nhập hoặc Hết thời gian tồn tại Session user đăng nhập, thì tạo lại
					httpSession.setAttribute("username", adminAccount.getUsername());
					httpSession.setMaxInactiveInterval(60*60*24);
				}
			}
			else {
				for (AdminAccountMessage adminAccountMessage : AdminAccountMessage.values()) {
					if (result == adminAccountMessage.getId()) {
						message = adminAccountMessage.getMessage();
						
						break;
					}
				}
			}
		}
		
		return view(modelMap, viewName, pageName, message);
	}
	
	@GetMapping("category")
	public String category(ModelMap modelMap) {
		
		modelMap.addAttribute("category", new Category());
		
		return "admin/category";
	}
	
	@PostMapping("category/imageUpload")
	@ResponseBody
	public String imageUpload(@RequestParam("upload") MultipartFile fileUpload, @RequestParam("CKEditor") String ckEdtitor,
			@RequestParam("CKEditorFuncNum") String callback, @RequestParam("langCode") String langCode, HttpServletRequest httpServletRequest) {
		// Lấy tên của ảnh tải lên
		String fileName = fileUpload.getOriginalFilename();
		// Lấy tên đuôi ảnh tải lên jpg hoặc png
		String fileExtension = FilenameUtils.getExtension(fileName);
		// Đường dẫn lưu ảnh trên Server
		String path = "";
		String pathDestination = "";
		// URL ảnh trả về Client
		String imageURL = "";
		// Nội dung trả về Client sau khi gửi ảnh đến Server
		String response = "";
		
		try {
			if (!isNullOrEmpty(fileExtension)) {
				if (fileExtension.trim().equalsIgnoreCase("jpg") || fileExtension.trim().equalsIgnoreCase("png")) {
					// Vị trí lưu ảnh trên Server được upload lên từ Client
					path = "/resources/images/upload/" + fileName;
					pathDestination = servletContext.getRealPath(path);
					File fileDestination = new File(pathDestination);
					
					// Đổ dữ liệu file ảnh từ Client vào dữ liệu file ảnh trên Server
					fileUpload.transferTo(fileDestination);
					
					String scheme = httpServletRequest.getScheme();
					String serverName = httpServletRequest.getServerName();
					String serverPort = String.valueOf(httpServletRequest.getServerPort());
					
					imageURL = scheme.concat("://").concat(serverName).concat(":").concat(serverPort).concat(path);
					System.out.println("pathDestination: " + pathDestination);
					System.out.println("imageURL: " + imageURL);
					
					// Upload file ảnh này ngược lại URL trên CKEditor
					StringBuffer stringBuffer = new StringBuffer();
					stringBuffer.append("<script type='text/javascript'>function delay(n) {return new Promise(done => { setTimeout(() => {");
					stringBuffer.append("window.parent.CKEDITOR.tools.callFunction(");
					stringBuffer.append(callback);
					stringBuffer.append(",'");
					stringBuffer.append(imageURL);
					stringBuffer.append("','Success');");
					stringBuffer.append("}, n);});}");
					stringBuffer.append("delay(10000);");
					stringBuffer.append("</script>");
					
					response = stringBuffer.toString();
				}
			}
		}
		catch (Exception e) {
			System.out.println(e.getMessage());
		}
		
		return response;
	}
	
	@PostMapping("category/insertCategory")
	public String insertCategory(@ModelAttribute("category") Category category) {
		String name = category.getName();
		String description = category.getDescription();
		System.out.println("Category: " + name + " - " + description);
		
		return "admin/category";
	}
	
	@GetMapping("department")
	public String department(ModelMap modelMap) {
		viewName = "admin/index";
		pageName = "department.jsp";
		message = "";
		
		departmentService = new DepartmentService();
		List<Department> listDepartment = departmentService.getListDepartmentExisting();
		if (listDepartment == null || (listDepartment != null && listDepartment.size() == 0)) {
			message = "Không có dữ liệu";
		}
		else {
			modelMap.addAttribute("listDepartment", listDepartment);
		}
		
		modelMap.addAttribute("department", new Department());
		
		return view(modelMap, viewName, pageName, message);
	}
	
	@GetMapping("staffPermission")
	public String staffPermission(ModelMap modelMap) {
		viewName = "admin/index";
		pageName = "staffpermission.jsp";
		message = "";
		
		return view(modelMap, viewName, pageName, message);
	}
	
	private String view(ModelMap modelMap, String viewName, String...contents) {
		if (contents.length > 0) {
			String pageName = contents[0];
			String message = contents[1];
			
			modelMap.addAttribute("pageName", pageName);
			modelMap.addAttribute("message", message);
		}
		
		return viewName;
	}
	
	private boolean isNullOrEmpty(String content) {
		if (content != null && content.trim() != "") {
			return false;
		}
		
		return true;
	}
}
