package vn.com.anhtraixunau.admin.api.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import vn.com.anhtraixunau.enums.DepartmentMessage;
import vn.com.anhtraixunau.models.Department;
import vn.com.anhtraixunau.services.DepartmentService;

@RestController
@RequestMapping("api/admin/department")
public class APIAdminDepartmentController {
	private DepartmentService departmentService;
	
	@GetMapping("getListDepartmentExisting")
	public Map<String, Object> getListDepartmentExisting() {
		Map<String, Object> response = new HashMap<String, Object>();
		List<Department> listDepartment = new ArrayList<Department>();
		
		try {
			departmentService = new DepartmentService();
			listDepartment = departmentService.getListDepartmentExisting();
			
			response.put("listDepartment", listDepartment);
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		return response;
	}
	
	@GetMapping("getDepartmentById/{id}")
	public Map<String, String> getDepartmentById(@PathVariable("id") Integer id) {
		Map<String, String> response = new HashMap<String, String>();
		Department department = new Department();
		departmentService = new DepartmentService();
		
		try {
			department = departmentService.getDepartmentById(id);
			
			if (department.getId() != 0) {
				response.put("departmentId", String.valueOf(department.getId()));
				response.put("departmentName", department.getName());
				response.put("departmentCreateUser", department.getCreateUser());
				response.put("departmentCreateDate", String.valueOf(department.getCreateDate()));
			}
			else {
				response.put("message", "Không có thông tin phòng ban");
			}
		}
		catch (Exception e) {
			e.printStackTrace();
			
			response.put("message", "Lỗi lấy thông tin phòng ban");
		}
		
		return response;
	}
	
	/**
	 * Hàm này sẽ lấy Id của phòng ban để xác định việc thêm hoặc sửa thông tin phòng ban
	 * <p>Id == -1: Thêm</p>
	 * <p>Id <> -1: Sửa</p>
	 * @param department
	 * Thông tin phòng ban
	 * @return
	 * Nội dung thêm hoặc sửa thông tin phòng ban
	 */
	@PostMapping("completeDepartmentEdit")
	public Map<String, Object> completeDepartmentEdit(@RequestBody Department department, HttpServletRequest httpServletRequest) {
		Map<String, Object> response = new HashMap<String, Object>();
		departmentService = new DepartmentService();
		
		try {
			System.out.println("departmentId: " + department.getId() + " - departmentName: " + department.getName() + " - userName: " + httpServletRequest.getSession().getAttribute("username"));
			int departmentId = department.getId();
			String departmentName = department.getName();
			String userName = (String) httpServletRequest.getSession().getAttribute("username");
			
			if (departmentId == DepartmentMessage.DOES_NOT_EXISTS_DEPARTMENTID.getId()) {
				departmentService.insertDepartment(departmentName, userName);
			}
			else {
				departmentService.updateDepartment(departmentId, departmentName, userName);
			}
			
			List<Department> listDepartment = new ArrayList<Department>();
			listDepartment = departmentService.getListDepartmentExisting();
			
			response.put("departmentId", department.getId());
			response.put("listDepartment", listDepartment);
		}
		catch (Exception e) {
			e.printStackTrace();
			
			response.put("message", "Lỗi thêm/sửa thông tin phòng ban");
		}
		
		return response;
	}
	
	@PostMapping("deleteDepartmentById/{id}")
	public Map<String, Object> deleteDepartmentById(@PathVariable("id") Integer id, HttpServletRequest httpServletRequest) {
		Map<String, Object> response = new HashMap<String, Object>();
		departmentService = new DepartmentService();
		
		try {
			String userName = (String) httpServletRequest.getSession().getAttribute("username");
			departmentService.deleteDepartmentById(id, userName);
			
			List<Department> listDepartment = new ArrayList<Department>();
			listDepartment = departmentService.getListDepartmentExisting();
			
			response.put("listDepartment", listDepartment);
		}
		catch (Exception e) {
			e.printStackTrace();
			
			response.put("message", "Lỗi xóa thông tin phòng ban");
		}
		
		return response;
	}
	
}
