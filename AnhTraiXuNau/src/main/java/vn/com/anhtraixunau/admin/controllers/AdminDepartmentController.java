package vn.com.anhtraixunau.admin.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import vn.com.anhtraixunau.enums.DepartmentMessage;
import vn.com.anhtraixunau.models.Department;
import vn.com.anhtraixunau.services.DepartmentService;

@Controller
@RequestMapping("admin/department")
public class AdminDepartmentController {
	private DepartmentService departmentService;
	private String viewName;
	private String pageName;
	private String message;
	
	/**
	 * Hàm này sẽ kiểm tra xem giá trị Id của Department truyền qua. 
	 * Insert (Id == -1)
	 * Update (Id <> -1),
	 * @param department
	 * @param bindingResult
	 * @param modelMap
	 * @return
	 */
	@PostMapping("insertOrUpdateDepartment")
	public String insertOrUpdateDepartment(@Validated @ModelAttribute("department") Department department, BindingResult bindingResult, ModelMap modelMap, HttpServletRequest httpServletRequest) {
		viewName = "admin/department";
		pageName = "";
		message = "";
		
		if (bindingResult.hasErrors()) {
			pageName = "error.jsp";
		}
		else {
			departmentService = new DepartmentService();
			
			if (department.getId() == null) {
				message = "ID is NULL";
				pageName = "error.jsp";
			}
			else {
				int departmentId = department.getId();
				String departmentName = department.getName();
				String userName = (String) httpServletRequest.getSession().getAttribute("username");
				
				if (departmentId == DepartmentMessage.DOES_NOT_EXISTS_DEPARTMENTID.getId()) {
					departmentService.insertDepartment(departmentName, userName);
				}
				else {
					departmentService.updateDepartment(departmentId, departmentName, userName);
				}
			}
			
			System.out.println("id: " + department.getId() + " - name: " + department.getName());
		}
		
		return view(modelMap, viewName, pageName, message);
	}
	
	private String view(ModelMap modelMap, String viewName, String...contents) {
		if (contents.length > 0) {
			String pageName = contents[0];
			String message = contents[1];
			
			modelMap.addAttribute("pageName", pageName);
			modelMap.addAttribute("message", message);
		}
		
		return viewName;
	}
}
