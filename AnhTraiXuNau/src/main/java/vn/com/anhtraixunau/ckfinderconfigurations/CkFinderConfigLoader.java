package vn.com.anhtraixunau.ckfinderconfigurations;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.inject.Named;

import com.cksource.ckfinder.config.Config;
import com.cksource.ckfinder.config.loader.ConfigLoader;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;

@Named
public class CkFinderConfigLoader implements ConfigLoader {

	@Override
	public Config loadConfig() throws Exception {
		ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
		Path path = Paths.get("src/main/resources/ckfinder.yml");
		
		return mapper.readValue(Files.newInputStream(path), Config.class);
	}

}
