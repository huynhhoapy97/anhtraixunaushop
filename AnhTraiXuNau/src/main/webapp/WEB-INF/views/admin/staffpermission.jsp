<%@ page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html ng-app="StaffPermissionApp">
	<head>
		<meta charset="utf-8">
		<title>Staff Permission</title>
		<link href="resources/select2/select2.min.css" rel="stylesheet" />
	</head>
	<body ng-controller="StaffPermissionController">
		<div>
			<div>
				<!-- <select id="listDepartment" style="width: 200px;">
					<option ng-repeat="department in staffPermission.listDepartment">{{department.name}}</option>
				</select> -->
				<select ng-options="department.name for department in staffPermission.listDepartment track by department.id" ng-model="department" ng-change="checkOption()" id="listDepartment" style="width: 200px;"></select>
			</div>
			<div>
				<input type="text" ng-model="staffPermission.id" id="staffPermissionId" ng-show="false" />
				<input type="text" ng-model="staffPermission.name" id="staffPermissionName" placeholder="Nhập tên quyền thao tác" />
			</div>
			<div>
				<button ng-model="staffPermission.isInsert" ng-click="completeStaffPermissionEdit()">{{staffPermissionAction}}</button>
				<button ng-show="!staffPermission.isInsert" ng-click="deleteStaffPermissionById()">{{staffPermissionDelete}}</button>
				<button ng-show="!staffPermission.isInsert" ng-click="cancelStaffPermissionEdit()">{{staffPermissionCancel}}</button>
			</div>
		</div>
		<hr>
		<div>
			<table style="border: 1px solid black;">
				<tr>
					<th style="border: 1px solid black;">Bộ phận</th>
					<th style="border: 1px solid black;">Quyền thao tác</th>
					<th style="border: 1px solid black;">Ngày tạo</th>
				</tr>
				<tr ng-repeat="staffPermission in staffPermission.listStaffPermission" ng-click="getStaffPermissionById(staffPermission.id)">
					<td style="border: 1px solid black;">{{staffPermission.departmentName}}</td>
					<td style="border: 1px solid black;">{{staffPermission.name}}</td>
					<td style="border: 1px solid black;">{{staffPermission.createDate | date: "dd/MM/yyyy"}} - {{staffPermission.createUser}}</td>
				</tr>
			</table>
		</div>
		
		<script type="text/javascript" src="resources/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/angular.min.js"></script>
		<script type="text/javascript" src="resources/js/staffpermission.js"></script>
		<script type="text/javascript" src="resources/select2/select2.min.js"></script>
	</body>
</html>