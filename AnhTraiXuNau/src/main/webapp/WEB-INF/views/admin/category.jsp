<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html>
	<head>
		<base href="${pageContext.request.contextPath}/">
		<meta charset="utf-8">
		<title>Quản lý loại Supplement</title>
	</head>
	<body>
		<form:form method="POST" action="admin/category/insertCategory" modelAttribute="category" >
			<div>
				<form:label path="name">Tên loại:</form:label>
				<form:input type="text" path="name" />
			</div>
			<div>
				<form:label path="description">Mô tả:</form:label>
				<form:textarea path="description" rows="5" cols="15" id="comment"></form:textarea>
			</div>
			<div>
				<button id="btnClick">Thêm</button>
			</div>
		</form:form>
		
		<script type="text/javascript" src="resources/ckeditor/ckeditor.js"></script>
		<script type="text/javascript" src="resources/ckfinder/ckfinder.js"></script>
	</body>
	
	<script>
		var ckeditor = CKEDITOR.replace('comment', {
			filebrowserImageUploadUrl: 'category/imageUpload',
			filebrowserUploadMethod: 'form'
		});
		//CKFinder.setupCKEditor(ckeditor, '/resources/ckfinder/');
		
		document.getElementById("btnClick").addEventListener("click", function(){
			alert("Comment content: " + ckeditor.getData());
		});
	</script>
</html>