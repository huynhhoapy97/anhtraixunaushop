<%@ page language="java" pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jstl/core_rt" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html>
<html ng-app="DepartmentApp">
	<head>
		<meta charset="utf-8">
		<title>Department</title>
	</head>
	<body ng-controller="DepartmentController">
		<div>
			<%-- <form:form method="POST" action="department/insertOrUpdateDepartment" modelAttribute="department">
				<div>
					<form:input type="text" path="id" ng-model="department.id" id="departmentId" ng-show="false" />
					<form:input type="text" path="name" ng-model="department.name" id="departmentName" placeholder="Nhập tên bộ phận" />
				</div>
				<div>
					<button ng-model="department.isInsert">{{departmentAction}}</button>
					<input type="button" ng-show="!department.isInsert" ng-click="cancelDepartmentEdit()" ng-value="departmentCancel" />
				</div>
			</form:form> --%>
			
			<div>
				<div>
					<input type="text" ng-model="department.id" id="departmentId" ng-show="false" />
					<input type="text" ng-model="department.name" id="departmentName" placeholder="Nhập tên bộ phận" />
				</div>
				<div>
					<button ng-model="department.isInsert" ng-click="completeDepartmentEdit()">{{departmentAction}}</button>
					<button ng-show="!department.isInsert" ng-click="deleteDepartmentById()">{{departmentDelete}}</button>
					<button ng-show="!department.isInsert" ng-click="cancelDepartmentEdit()">{{departmentCancel}}</button>
				</div>
			</div>
		</div>
		<hr>
		<div>
			<table style="border: 1px solid black;">
				<tr>
					<th style="border: 1px solid black;">Bộ phận</th>
					<th style="border: 1px solid black;">Ngày tạo</th>
				</tr>
				<%-- <c:forEach var="department" items="${listDepartment}">
					<tr ng-click="getDepartmentById(${department.id})">
						<td style="border: 1px solid black;">Chúc mừng năm mới</td>
						<td style="border: 1px solid black;">${department.name}</td>
						<td style="border: 1px solid black;">${department.createDate} - ${department.createUser}</td>
						<td style="border: 1px solid black;">3</td>
					</tr>
				</c:forEach> --%>
				<tr ng-repeat="department in department.listDepartment" ng-click="getDepartmentById(department.id)">
					<td style="border: 1px solid black;">{{department.name}}</td>
					<td style="border: 1px solid black;">{{department.createDate | date: "dd/MM/yyyy"}} - {{department.createUser}}</td>
				</tr>
			</table>
		</div>
		
		<script type="text/javascript" src="resources/jquery/jquery.min.js"></script>
		<script type="text/javascript" src="resources/angularjs/angular.min.js"></script>
		<script type="text/javascript" src="resources/js/department.js"></script>
	</body>
</html>