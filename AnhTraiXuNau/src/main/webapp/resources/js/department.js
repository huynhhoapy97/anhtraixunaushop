var app = angular.module('DepartmentApp', []);
app.controller('DepartmentController', function($scope, $http){
	const DEPARTMENT = {
		DOES_NOT_EXISTS_DEPARTMENTID: -1,
		DOES_NOT_EXISTS_DEPARTMENTNAME: '',
		DEPARTMENT_ACTION_SAVE: 'Save',
		DEPARTMENT_ACTION_UPDATE: 'Update',
		DEPARTMENT_ACTION_CANCEL: 'Cancel',
		DEPARTMENT_ACTION_DELETE: 'Delete',
		DEPARTMENT_ISINSERT_ON: true,
		DEPARTMENT_ISINSERT_OFF: false
	};
	
	$scope.department = {};
	
	$scope.init = function() {
		$scope.departmentAction = DEPARTMENT.DEPARTMENT_ACTION_SAVE;
		$scope.department.isInsert = DEPARTMENT.DEPARTMENT_ISINSERT_ON;
		$scope.department.id = DEPARTMENT.DOES_NOT_EXISTS_DEPARTMENTID;
		
		$scope.getListDepartmentExisting();
	}
	
	$scope.getDepartmentById = function(departmentId) {
		let url = 'api/admin/department/getDepartmentById/' + departmentId;
		
		/*$.ajax({
			url: url,
			data: data,
			dataType: 'json',
			contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
			method: 'GET',	
			success: function(result) {
				if (result) {
					$scope.department.name = result.departmentName;
					$scope.$digest();
				}
				else {
					alert(result.message);
				}
			},
			error: function(result) {
				alert(result.message);
			}
		});*/
		
		$http({
			 method: 'GET',
			 url: url
		}).then(function successCallback(response) {
			if (response.data) {
				if (response.data.message) {
					alert(response.data.message);	
				}
				else {
					$scope.department.id = response.data.departmentId;
					$scope.department.name = response.data.departmentName;
					
					$scope.departmentAction = DEPARTMENT.DEPARTMENT_ACTION_UPDATE;
					$scope.departmentDelete = DEPARTMENT.DEPARTMENT_ACTION_DELETE;
					$scope.departmentCancel = DEPARTMENT.DEPARTMENT_ACTION_CANCEL;
					$scope.department.isInsert = DEPARTMENT.DEPARTMENT_ISINSERT_OFF;
				}
			}
			else {
				alert('Error getDepartmentById');
			}
		}, function errorCallback(response) {
			console.log(JSON.stringify(response));
			alert('Error Callback getDepartmentById');
		});
	}
	
	$scope.cancelDepartmentEdit = function() {
		$scope.department.id = DEPARTMENT.DOES_NOT_EXISTS_DEPARTMENTID;
		$scope.department.name = DEPARTMENT.DOES_NOT_EXISTS_DEPARTMENTNAME;
		
		$scope.departmentAction = DEPARTMENT.DEPARTMENT_ACTION_SAVE;
		$scope.department.isInsert = DEPARTMENT.DEPARTMENT_ISINSERT_ON;
	}
	
	$scope.completeDepartmentEdit = function() {
		let url = 'api/admin/department/completeDepartmentEdit';
		let data = JSON.stringify({
			id: $scope.department.id,
			name: $scope.department.name	
		});
		
		$http({
			method: 'POST',
			url: url,
			data: data
		}).then(function successCallback(response) {
			if (response.data) {
				$scope.departmentAction = DEPARTMENT.DEPARTMENT_ACTION_SAVE;
				$scope.department.isInsert = DEPARTMENT.DEPARTMENT_ISINSERT_ON;
				$scope.department.id = DEPARTMENT.DOES_NOT_EXISTS_DEPARTMENTID;
				$scope.department.name = DEPARTMENT.DOES_NOT_EXISTS_DEPARTMENTNAME;
				
				$scope.department.listDepartment = response.data.listDepartment;	
			}
			else {
				alert('Error completeDepartmentEdit');
			}
		}, function errorCallback(response) {
			console.log(JSON.stringify(response));
			alert('Error Callback completeDepartmentEdit')
		});
	}
	
	$scope.getListDepartmentExisting = function() {
		let url = 'api/admin/department/getListDepartmentExisting';
		
		$http({
			method: 'GET',
			url: url
		}).then(function successCallback(response) {
			if (response.data) {
				$scope.department.listDepartment = response.data.listDepartment;			
			}
			else {
				alert('Error getListDepartmentExisting');
			}
		}, function errorCallback(response) {
			console.log(JSON.stringify(response));
			alert('Error Callback getListDepartmentExisting')
		});
	}
	
	$scope.deleteDepartmentById = function() {
		let departmentId = $scope.department.id;
		let url = 'api/admin/department/deleteDepartmentById/' + departmentId;
		let warning = 'Are you sure to delete: ' + departmentId;
		
		if (confirm(warning) == true) {
			$http({
				method: 'POST',
				url: url
			}).then(function successCallback(response) {
				if (response.data) {
					$scope.departmentAction = DEPARTMENT.DEPARTMENT_ACTION_SAVE;
					$scope.department.isInsert = DEPARTMENT.DEPARTMENT_ISINSERT_ON;
					$scope.department.id = DEPARTMENT.DOES_NOT_EXISTS_DEPARTMENTID;
					$scope.department.name = DEPARTMENT.DOES_NOT_EXISTS_DEPARTMENTNAME;
					
					$scope.department.listDepartment = response.data.listDepartment;	
				}
				else {
					alert('Error deleteDepartmentById');
				}
			}, function errorCallback(response) {
				console.log(JSON.stringify(response));
				alert('Error Callback deleteDepartmentById')
			});
		}
	}
	
	$scope.init();
});