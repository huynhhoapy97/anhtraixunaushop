package vn.com.anhtraixunau.repositories;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;

import vn.com.anhtraixunau.connections.OracleConnection;
import vn.com.anhtraixunau.enums.AdminAccountMessage;
import vn.com.anhtraixunau.models.AdminAccount;

public class AdminAccountDAOImpl implements AdminAccountDAO {
	private OracleConnection oracleConnection;
	
	@Override
	public int checkAccountExists(String username, String password) {
		oracleConnection = new OracleConnection();
		
		try {
			Connection connection = oracleConnection.connectToOracle();
			oracleConnection.setConnection(connection);
			
			String query = "{call atxn_staff_account_chk(?,?)}";
			
			CallableStatement callableStatement = connection.prepareCall(query);
			oracleConnection.setCallableStatement(callableStatement);
			
			callableStatement.setString("i_username", username);
			callableStatement.registerOutParameter("o_result", Types.INTEGER);
			
			callableStatement.execute();
			
			int result = callableStatement.getInt("o_result");			
			
			return result;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return AdminAccountMessage.MESSAGE_ERROR_CHECKACCOUNTEXISTS.getId();
		}
		finally {
			oracleConnection.close();
		}
	}

	@Override
	public int changePassword(String username, String password) {
		oracleConnection = new OracleConnection();
		
		try {
			oracleConnection.setConnection(oracleConnection.connectToOracle());
			
			String query = "{call atxn_staff_account_upd(?,?,?)}";
			oracleConnection.setCallableStatement(oracleConnection.getConnection().prepareCall(query));
			
			oracleConnection.getCallableStatement().setString("i_username", username);
			oracleConnection.getCallableStatement().setString("i_password", password);
			oracleConnection.getCallableStatement().registerOutParameter("o_result", Types.INTEGER);
			
			oracleConnection.getCallableStatement().executeUpdate();
			
			int result = oracleConnection.getCallableStatement().getInt("o_result");
			
			return result;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return AdminAccountMessage.MESSAGE_ERROR_CHANGEPASSWORD.getId();
		}
		finally {
			oracleConnection.close();
		}
	}

	@Override
	public AdminAccount getInformationByUsername(String username) {
		oracleConnection = new OracleConnection();
		AdminAccount adminAccount = new AdminAccount();
		
		try {
			oracleConnection.setConnection(oracleConnection.connectToOracle());
			
			String query = "{call atxn_staff_account_get(?,?)}";
			oracleConnection.setCallableStatement(oracleConnection.getConnection().prepareCall(query));
			
			oracleConnection.getCallableStatement().setString("i_username", username);
			oracleConnection.getCallableStatement().registerOutParameter("o_result", Types.REF_CURSOR);
			
			oracleConnection.getCallableStatement().execute();
			
			ResultSet resultSet = (ResultSet) oracleConnection.getCallableStatement().getObject("o_result");
			while (resultSet.next()) {
				int id = resultSet.getInt("sa_id");
				String password = resultSet.getString("sa_password");
				
				adminAccount.setId(id);
				adminAccount.setPassword(password);
			}
			
			return adminAccount;
		}
		catch (Exception e) {
			e.printStackTrace();
			
			return adminAccount;
		}
		finally {
			oracleConnection.close();
		}
	}
}
