package vn.com.anhtraixunau.repositories;

import java.util.List;

import vn.com.anhtraixunau.models.Department;

public interface DepartmentDAO {
	public List<Department> getListDepartmentExisting();
	public Department getDepartmentById(int id);
	public int insertDepartment(String name, String userName);
	public int updateDepartment(int id, String name, String userName);
	public int deleteDepartmentById(int id, String userName);
}
