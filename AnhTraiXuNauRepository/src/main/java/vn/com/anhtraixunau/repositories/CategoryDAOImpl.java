package vn.com.anhtraixunau.repositories;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.Types;

import vn.com.anhtraixunau.connections.OracleConnection;

public class CategoryDAOImpl implements CategoryDAO{
	private OracleConnection oracleConnection;
	
	@Override
	public int insertCategory(String name, String description, String createUser) {
		oracleConnection = new OracleConnection();
		
		try {
			Connection connection = oracleConnection.connectToOracle();
			oracleConnection.setConnection(connection);
			
			String query = "{call atxn_category_ins(?,?,?,?)}";
			
			CallableStatement callableStatement = connection.prepareCall(query);
			oracleConnection.setCallableStatement(callableStatement);
			
			callableStatement.setString("i_name", name);
			callableStatement.setString("i_description", description);
			callableStatement.setString("i_createuser", createUser);
			callableStatement.registerOutParameter("o_result", Types.INTEGER);
			
			callableStatement.executeUpdate();
			
			int result = callableStatement.getInt("o_result");
			
			return result;
		}
		catch (Exception e) {
			return -1;
		}
		finally {
			oracleConnection.close();
		}
	}

}
