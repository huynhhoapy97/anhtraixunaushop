package vn.com.anhtraixunau.repositories;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import vn.com.anhtraixunau.connections.OracleConnection;
import vn.com.anhtraixunau.models.Department;

public class DepartmentDAOImpl implements DepartmentDAO {
	private OracleConnection oracleConnection;
	
	@Override
	public List<Department> getListDepartmentExisting() {
		oracleConnection = new OracleConnection();
		List<Department> listDepartment = new ArrayList<Department>();
		
		try {
			Connection connection = oracleConnection.connectToOracle();
			oracleConnection.setConnection(connection);
			
			String query = "{call atxn_department_get(?)}";
			CallableStatement callableStatement = connection.prepareCall(query);
			
			oracleConnection.setCallableStatement(callableStatement);
			
			oracleConnection.getCallableStatement().registerOutParameter("o_result", Types.REF_CURSOR);
			oracleConnection.getCallableStatement().execute();
			
			ResultSet resultSet = (ResultSet) oracleConnection.getCallableStatement().getObject("o_result");
			while(resultSet.next()) {
				int id = resultSet.getInt("d_id");
				String name = resultSet.getString("d_name");
				String createUser = resultSet.getString("d_createuser");
				Date createDate = resultSet.getDate("d_createdate");
				
				Department department = new Department();
				department.setId(id);
				department.setName(name);
				department.setCreateUser(createUser);
				department.setCreateDate(createDate);
				
				listDepartment.add(department);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			oracleConnection.close();
		}
		
		return listDepartment;
	}

	@Override
	public Department getDepartmentById(int id) {
		oracleConnection = new OracleConnection();
		Department department = new Department();
		
		try {
			Connection connection = oracleConnection.connectToOracle();
			oracleConnection.setConnection(connection);
			
			String query = "{call atxn_department_getbyid(?,?)}";
			CallableStatement callableStatement = connection.prepareCall(query);
			
			oracleConnection.setCallableStatement(callableStatement);
			
			oracleConnection.getCallableStatement().setInt("i_id", id);
			oracleConnection.getCallableStatement().registerOutParameter("o_result", Types.REF_CURSOR);
			oracleConnection.getCallableStatement().execute();
			
			ResultSet resultSet = (ResultSet) oracleConnection.getCallableStatement().getObject("o_result");
			while (resultSet.next()) {
				String name = resultSet.getString("d_name");
				String createUser = resultSet.getString("d_createuser");
				Date createDate = resultSet.getDate("d_createdate");
				
				department.setId(id);
				department.setName(name);
				department.setCreateUser(createUser);
				department.setCreateDate(createDate);
			}
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			oracleConnection.close();
		}
		
		return department;
	}

	@Override
	public int insertDepartment(String name, String userName) {
		oracleConnection = new OracleConnection();
		int result = -1;
		
		try {
			oracleConnection.setConnection(oracleConnection.connectToOracle());
			
			String query = "{call atxn_department_ins(?,?,?)}";
			oracleConnection.setCallableStatement(oracleConnection.getConnection().prepareCall(query));
			
			oracleConnection.getCallableStatement().setString("i_name", name);
			oracleConnection.getCallableStatement().setString("i_createuser", userName);
			oracleConnection.getCallableStatement().registerOutParameter("o_result", Types.INTEGER);
			
			oracleConnection.getCallableStatement().executeUpdate();
			
			result = oracleConnection.getCallableStatement().getInt("o_result");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			oracleConnection.close();
		}
		
		return result;
	}

	@Override
	public int updateDepartment(int id, String name, String userName) {
		oracleConnection = new OracleConnection();
		int result = -1;
		
		try {
			oracleConnection.setConnection(oracleConnection.connectToOracle());
			
			String query = "{call atxn_department_upd(?,?,?,?)}";
			oracleConnection.setCallableStatement(oracleConnection.getConnection().prepareCall(query));
			
			oracleConnection.getCallableStatement().setInt("i_id", id);
			oracleConnection.getCallableStatement().setString("i_name", name);
			oracleConnection.getCallableStatement().setString("i_updateuser", userName);
			oracleConnection.getCallableStatement().registerOutParameter("o_result", Types.INTEGER);
			
			oracleConnection.getCallableStatement().executeUpdate();
			
			result = oracleConnection.getCallableStatement().getInt("o_result");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			oracleConnection.close();
		}
		
		return result;
	}

	@Override
	public int deleteDepartmentById(int id, String userName) {
		oracleConnection = new OracleConnection();
		int result = -1;
		
		try {
			oracleConnection.setConnection(oracleConnection.connectToOracle());
			
			String query = "{call atxn_department_del(?,?,?)}";
			oracleConnection.setCallableStatement(oracleConnection.getConnection().prepareCall(query));
			
			oracleConnection.getCallableStatement().setInt("i_id", id);
			oracleConnection.getCallableStatement().setString("i_updateuser", userName);
			oracleConnection.getCallableStatement().registerOutParameter("o_result", Types.INTEGER);
			
			oracleConnection.getCallableStatement().executeUpdate();
			
			result = oracleConnection.getCallableStatement().getInt("o_result");
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		finally {
			oracleConnection.close();
		}
		
		return result;
	}

}
